;; This is an operating system configuration generated
;; by the graphical installer, and then customized by hand.
;; (define-module (c2 system config))

;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules
 (gnu)
 ;; for wifi firmware from nonguix
 (nongnu packages linux)
 (nongnu system linux-initrd)
 ;; for user default shell
 (gnu packages bash)
 ;; for android-udev-rules
 (gnu packages android)
 ;;for user-group
 (gnu system shadow)
 ;;for iwd
 (gnu services networking)
 ;; for virt-manager daemon
 (gnu services virtualization)
 ;; for docker
 (gnu services docker))

(use-service-modules cups desktop networking ssh xorg)

(operating-system
 ;; for nonguix
 (kernel linux)
 ;; (initrd microcode-initrd)

 ;; non guix missing wifi firmware for lenovo.thinkpad@x390
 (firmware (cons* iwlwifi-firmware
									%base-firmware))

 (locale "en_US.utf8")
 (timezone "Europe/Berlin")
 (keyboard-layout (keyboard-layout "us"))
 (host-name "c2")

 ;; The list of user accounts ('root' is implicit).
 (users (cons* (user-account
								(name "u0")
								(comment "u0")
								(group "users")
								(home-directory "/home/u0")
								(shell (file-append bash "/bin/bash"))
								(supplementary-groups
								 '("wheel" "netdev" "audio" "video"
									 "adbusers" ; for android adb
									 )))
							 %base-user-accounts))

 ;; create a new group
 ;; (groups
 ;; 	(cons*
 ;; 	 ;; for android ADB
 ;; 	 (user-group (name "adbusers"))
 ;; 	 %base-groups))


 ;; Packages installed system-wide.	 Users can also install packages
 ;; under their own account: use 'guix search KEYWORD' to search
 ;; for packages and 'guix install PACKAGE' to install a package.
 (packages
	(append
	 (list
		;; emacs and exwm
		(specification->package "emacs")
		(specification->package "emacs-exwm")
		(specification->package
		 "emacs-desktop-environment")

		;; https certificates
		(specification->package "nss-certs"))
	 %base-packages))

 ;; Below is the list of system services.	 To search for available
 ;; services, run 'guix system search KEYWORD' in a terminal.
 (services
	(append (list

					 ;; To configure OpenSSH, pass an 'openssh-configuration'
					 ;; record as a second argument to 'service' below.
					 (service openssh-service-type)

					 ;; for docker
					 (service docker-service-type)

					 ;; for bluetooth
					 (service bluetooth-service-type)

					 ;; virt-manager for vm
					 ;; https://guix.gnu.org/manual/en/html_node/Virtualization-Services.html
					 (service libvirt-service-type
										(libvirt-configuration
										 (unix-sock-group "libvirt")
										 (tls-port "16555")))


					 ;; udev rules for android adb
					 (udev-rules-service 'android android-udev-rules #:groups '("adbusers"))

					 ;; xorg
					 (set-xorg-configuration
						(xorg-configuration (keyboard-layout keyboard-layout))))

					;; This is the default list of services we
					;; are appending to.
					%desktop-services))

 ;; bootloader
 (bootloader (bootloader-configuration
							(bootloader grub-efi-bootloader)
							(targets (list "/boot/efi"))
							(keyboard-layout keyboard-layout)))

 ;; devices
 (mapped-devices (list (mapped-device
												(source (uuid "b0223d8d-4f12-4eeb-ac3a-69c38b3da769"))
												(target "cryptroot")
												(type luks-device-mapping))))

 ;; The list of file systems that get "mounted".	The unique
 ;; file system identifiers there ("UUIDs") can be obtained
 ;; by running 'blkid' in a terminal.
 (file-systems (cons* (file-system
											 (mount-point "/boot/efi")
											 (device (uuid "5C53-FB6F" 'fat32))
											 (type "vfat"))
											(file-system
											 (mount-point "/")
											 (device "/dev/mapper/cryptroot")
											 (type "ext4")
											 (dependencies mapped-devices)) %base-file-systems)))
