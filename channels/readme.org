#+TITLE: Guix channels
Reference and use guix channels (git repositories, remote or local),
to search and install packages from.

#+begin_quote
This config only sources from remote channels, even for its local
package definitions. Switch the comment lines in the channel
definition to use the local packages.
#+end_quote

* Update channels
Pull channels to update the packages definitions in the local store.
#+begin_src bash
# guix pull --channels=<path_to_channels_description>
guix pull -C ./channels.scm
#+end_src

* Pin channels
nWrite the current guix configuration channels to the user's guix
config file.
#+begin_src bash
guix describe --format=channels > $HOME/.config/guix/channels.scm
#+end_src

This way guix will use a specific commit in the git repository of each
channel, as the latest version it will fetch updates for the packages
when using the =guix pull= command. We can use it without argument,
since the pinned channels description is located in the user's guix
config folder.

* Custom packages from custom channel
This repository is a custom (personal) guix channel to import packages
described in the =/packages/= folder; see [[https://guix.gnu.org/en/cookbook/en/guix-cookbook.pdf][docs]] (=#2/Packaging > Setup >
channels=).

Right now only used for testing the feature.

** Usage
For example to list a/the package(s) in a/this guix channel.
#+begin_src bash
# guix <show | build | install> -L <GIT_URL_OR_PATH_TO_REPO> <guix-module>
guix show -L git@gitlab.com/ugrp/guix my-hello
guix build -L git@gitlab.com/ugrp/guix my-hello
guix install -L git@gitlab.com/ugrp/guix my-hello
#+end_src* 
