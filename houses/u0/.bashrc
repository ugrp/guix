alias u0pass="env PASSWORD_STORE_DIR=~/.password-store/u0 pass"
alias i4kpass="env PASSWORD_STORE_DIR=~/.password-store/i4k pass"

# Bash initialization for interactive non-login shells and
# for remote shells (info "(bash) Bash Startup Files").

# Export 'SHELL' to child processes.  Programs such as 'screen'
# honor it and otherwise use /bin/sh.
export SHELL

if [[ $- != *i* ]]
then
    # We are being invoked from a non-interactive shell.  If this
    # is an SSH session (as in "ssh host command"), source
    # /etc/profile so we get PATH and other essential variables.
    [[ -n "$SSH_CLIENT" ]] && source /etc/profile

    # Don't do anything else.
    return
fi

# Source the system-wide file.
[ -f /etc/bashrc ] && source /etc/bashrc

alias ls='ls -p --color=auto'
alias ll='ls -l'
alias grep='grep --color=auto'
alias ip='ip -color=auto'

# environment variable
export EDITOR="emacsclient"
export VISUAL=$EDITOR
export BROWSER="mullvad"

# set right alt as compose key
setxkbmap -option compose:ralt

# set caps lock as control key
setxkbmap -option ctrl:swapcaps

# to source necessary env vars
GUIX_PROFILE="/home/u0/.guix-profile"
. "$GUIX_PROFILE/etc/profile"

