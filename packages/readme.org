#+TITLE: guix packaging
How to use a personal guix channel to distribute installations of the
missing packages we need.

* Define custom guix channel
See the [[file:readme.org][channels folder]] in this repo, for how to distribute packages
using a custom personal guix channel (like guix or nonguix, with our
personal packages).

* Create a new package
A new package starts with a new file, =my-package.scm= which can be
stored in the =./package/= folder of this repo, for organisation.

* Hash of downloaded artifacts
When creating a new package, we need to specify a =sha256= hash for the
verification of the downloaded source.
#+begin_src guix
(sha256
  (base32
    "0c0xs5zwcj41qp03ac06lafmhrc58byn0mw6czbw533an1xi0zvb"))
#+end_src

To get the value of the hash, we can use the =guix download= command,
which last output is the hashed value for the downloaded artifacts.

#+begin_src bash
# guix download <package_artifacts_url>
guix download https://pkgs.tailscale.com/stable/tailscale_1.62.0_amd64.tgz
#+end_src

#+begin_export html
Starting download of /tmp/guix-file.5Y33NL
From https://pkgs.tailscale.com/stable/tailscale_1.62.0_amd64.tgz...
following redirection to `https://dl.tailscale.com/stable/tailscale_1.62.0_amd64.tgz'...
 …2.0_amd64.tgz  25.9MiB                                                                                                                             9.4MiB/s 00:03 ▕██████████████████▏ 100.0%
/gnu/store/swqcfy2m36l9m7015ax5828n7yba8vlr-tailscale_1.62.0_amd64.tgz
0c0xs5zwcj41qp03ac06lafmhrc58byn0mw6czbw533an1xi0zvb
#+end_export

We can use this value in the package definition for the "source's" origin.

* Install package from file
#+begin_src bash
# guix package -f <path_to_file.scm>
guix package -f tailscale.scm
#+end_src
