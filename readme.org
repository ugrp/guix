* guix base
How to manage and and install guix package, home, system, channel.

** guix systems
All systems are in the =/systems/= directory. The configuration files
describe an operating system (guix) installed on a machine, and the
packages and services installed system-wide.

*** Install
To install a new system. follow the guix installation guide, from the
iso file "burned" on the installation medium (usb thumbdrive).

It is possible to use the default mainline guix image to install guix
as a new OS system, even if there are missing (non-free) firmware,
using *usb tethering* to connect to the internet via a phone (connected
itself to a wifi).

When the system is booting, The system's configuration can be *updated*
to include the non free firmware.

*** Update
To update a system from a definition file, for example with this
repo's configurations.

#+begin_src bash
# guix pull -C <CHANNELS_FILE>
guix pull -C ./channels/channels.scm

# guix system reconfigure <SYSTEM_FILE>
guix system reconfigure ./systems/$(hostname)/config.scm
#+end_src

The system file is usually found after an installation in
=/etc/config.scm= and a system file from this repo can used in place of
the default one (if the channels are correctly setup, and the guix
image has access to the required packages/firmware).

It is also possible to use a symbolic link to be able to use =guix
system reconfigure= without argument, if the default system
configuration file points to the correct file.

#+begin_src bash
ln -s ./systems/$(hostname)/config.scm /etc/config.scm
#+end_src

** guix home
To backup, import, the user's home folder it is possible to run:
#+begin_src bash

# for example
# guix home import <DESTINATION_FOLDER>
guix home import ./houses/$(whoami)

# guix home reconfigure <ORIGIN_CONFIG_FILE>
guix home reconfigure ./houses/$(whoami)/home-configuration.scm
#+end_src

** guix packages
To install packages.
#+begin_src bash
guix search <package_name>
guix package --list-installed

# install 
guix install <package_name>
guix package -i <package_name>
#+end_src

To have them referenced in the guix home config, use the guix home
import command.

To update/upgrade all the installed packages:
#+begin_src bash
guix package -u
#+end_src

** Info and tools
With a default mainline guix install image.

*** usb tethering
Using a phone with android, plug the phone to the computer with a usb
cable, activate "usb tethering" in the android network settings (close to
wifi/network/connections).

The guix computer should detect the phone as a wired interface (like
ethernet rj45 link).

*** wifi

In a file =wifi.conf= with content:
#+begin_src text
network={
  ssid="MY-SSID"
  key_mgmt=WPA-PSK
  psk="the network's secret passphrase"
}
#+end_src

Can be used whith the commands (found in =info guix= installation section):
#+begin_src
ifconfig WLAN_INTERFACE up
dhclient -v WLAN_INTERFACE
wpa_supplicant -c wifi.conf -i WLAN_INTERFACE -B
ping 9.9.9.9
#+end_src

Where =WLAN_INTERFACE= is the result of =ip a= or =ifconfig= for the wifi
interface we want to connect with.

Also can use =iwtl= that can be installed with guix and started as a
daemon with ...

** Docs
- https://guix.gnu.org/manual/devel/en/guix.html#index-file_002d_003eudev_002drule
	
** Guile
An example =guile= program could be =hello.scm= with content =(display
"Hello, World!\n")= and run with the =guile hello.scm= command.
